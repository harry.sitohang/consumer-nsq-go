package main

import (
	"github.com/nsqio/go-nsq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/prakerja/consumer-nsq-go/config"
	"gitlab.com/prakerja/consumer-nsq-go/handlers"
	"time"
)

func main() {
	log.SetFormatter(&log.JSONFormatter{
		TimestampFormat: time.RFC3339,
	})
	log.SetReportCaller(true)
	log.SetLevel(log.InfoLevel)

	cfg := config.Get()

	nsqConfig := nsq.NewConfig()
	nsqConfig.MaxAttempts = cfg.MaxRequeueAttempt
	consumer, err := nsq.NewConsumer(cfg.TopicName, cfg.ConsumerChannelName, nsqConfig)
	if err != nil {
		panic(err)
	}

	consumer.ChangeMaxInFlight(cfg.MaxInflight)

	handler := handlers.NewHandler(cfg.ServiceName)
	defer func() {
		handler.Close()
	}()

	consumer.AddConcurrentHandlers(
		handler,
		cfg.NsqConcurrency,
	)

	blocking := make(chan bool)
	err = consumer.ConnectToNSQD(cfg.NsqServer)
	if err != nil {
		panic(err)
	}

	<-blocking
}
