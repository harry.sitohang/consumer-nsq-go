# NSQ consumer async

- Async NSQ consumer by using `DisableAutoResponse`
- Go routines pipeline channels with config `MaxInflight`
- NSQ Requeue with config `MaxRequeueAttempt`
- Use `logrus` logging with context 

![image info](./log.png) 