package config

import (
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	ServiceName         string `envconfig:"SERVICE_NAME" default:"test-service"`
	NsqServer           string `envconfig:"NSQ_SERVER" default:""`
	MaxInflight         int    `envconfig:"MAX_INFLIGHT" default:"5"`
	NsqConcurrency      int    `envconfig:"NSQ_CONCURRENCY" default:"1"`
	MaxRequeueAttempt   uint16 `envconfig:"MAX_REQUEUE_ATTEMPT" default:"5"`
	RequeueDelay        int    `envconfig:"REQUEUE_DELAY" default:"1"` // in seconds
	TopicName           string `envconfig:"TOPIC_NAME" default:"topic-test-123"`
	ConsumerChannelName string `envconfig:"CONSUMER_CHANNEL_NAME" default:"forever-alone"`
}

// Get ...
func Get() Config {
	cfg := Config{}
	envconfig.MustProcess("", &cfg)

	return cfg
}
