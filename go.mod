module gitlab.com/prakerja/consumer-nsq-go

go 1.22.0

require (
	github.com/google/uuid v1.6.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/nsqio/go-nsq v1.1.0
	github.com/sirupsen/logrus v1.9.3
)

require (
	github.com/golang/snappy v0.0.1 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
