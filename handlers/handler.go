package handlers

import (
	"context"
	"github.com/google/uuid"
	"github.com/nsqio/go-nsq"
	log "github.com/sirupsen/logrus"
	log2 "gitlab.com/prakerja/consumer-nsq-go/log"
	"time"
)

type MessageT[T any] struct {
	Message *nsq.Message
	Data    T
	Ctx     context.Context
}

type Error struct {
	CurrentError error
	Ctx          context.Context
}

func (e Error) Log() {
	if e.Ctx != nil {
		logEntry := log2.GetLoggerFromContext(e.Ctx)
		logEntry.Error(e)
		return
	}

	log.Error(e)
}

func (e Error) Error() string {
	if e.CurrentError != nil {
		return e.CurrentError.Error()
	}

	return ""
}

type Handler struct {
	serviceName string
	out1Ch      chan *nsq.Message
	out2Ch      chan MessageT[string]
	out3Ch      chan MessageT[bool]
	out4Ch      chan MessageT[int]
	doneCh      chan MessageT[int64] // > 0 REQ, 0 FIN

	errCh chan error
}

func NewHandler(serviceName string) *Handler {
	h := &Handler{
		serviceName: serviceName,
		out1Ch:      make(chan *nsq.Message, 1),
		out2Ch:      make(chan MessageT[string], 1),
		out3Ch:      make(chan MessageT[bool], 1),
		out4Ch:      make(chan MessageT[int], 1),
		doneCh:      make(chan MessageT[int64], 1),
		errCh:       make(chan error, 1),
	}

	h.init()

	return h
}

func (h Handler) Close() {
	close(h.out1Ch)
	close(h.out2Ch)
	close(h.out3Ch)
	close(h.out4Ch)
	close(h.doneCh)
	close(h.errCh)
}

func (h Handler) init() {
	go func() {
		for message := range h.out1Ch {
			func() {
				logEntry := log.NewEntry(log.New())
				logEntry = logEntry.WithField("svc", h.serviceName)
				logEntry = logEntry.WithField("guid", uuid.New().String())
				logEntry.Infof("out1Ch: %s", string(message.Body))

				time.Sleep(1 * time.Second)

				h.out2Ch <- MessageT[string]{
					Message: message,
					Data:    "abc",
					Ctx:     log2.ContextWithLogger(context.TODO(), logEntry),
				}
			}()
		}
	}()

	go func() {
		for out2 := range h.out2Ch {
			func() {
				logEntry := log2.GetLoggerFromContext(out2.Ctx)
				logEntry.Infof("out2Ch: %+v", out2.Data)

				time.Sleep(1 * time.Second)

				h.out3Ch <- MessageT[bool]{
					Message: out2.Message,
					Data:    true,
					Ctx:     out2.Ctx,
				}
			}()
		}
	}()

	go func() {
		for out3 := range h.out3Ch {
			func() {
				logEntry := log2.GetLoggerFromContext(out3.Ctx)
				logEntry.Infof("out3Ch: %+v", out3.Data)

				time.Sleep(1 * time.Second)

				h.out4Ch <- MessageT[int]{
					Message: out3.Message,
					Data:    1,
					Ctx:     out3.Ctx,
				}
			}()
		}
	}()

	go func() {
		for out4 := range h.out4Ch {
			func() {
				logEntry := log2.GetLoggerFromContext(out4.Ctx)
				logEntry.Infof("out4Ch: %+v", out4.Data)

				defer func() {
					// REQUEUE
					h.doneCh <- MessageT[int64]{
						Message: out4.Message,
						Data:    60, // REQ with delay 5 seconds
						Ctx:     out4.Ctx,
					}
				}()

				time.Sleep(1 * time.Second)
			}()
		}
	}()

	go func() {
		for done := range h.doneCh {
			func() {
				logEntry := log2.GetLoggerFromContext(done.Ctx)
				if done.Data == 0 {
					logEntry.Infof("FIN")
					done.Message.Finish()
					return
				}

				done.Message.RequeueWithoutBackoff(time.Duration(done.Data) * time.Second)
				logEntry.Infof("RequeueWithoutBackoff attempt-%d", done.Message.Attempts+1)
			}()
		}
	}()

	go func() {
		for err := range h.errCh {
			func() {
				if e, ok := err.(Error); ok {
					e.Log()
					return
				}

				log.Error(err)
			}()
		}
	}()
}

func (h Handler) HandleMessage(message *nsq.Message) error {
	message.DisableAutoResponse()
	h.out1Ch <- message
	return nil
}
