package publish_test

import (
	"fmt"
	"github.com/nsqio/go-nsq"
	"os"
	"testing"
	"time"
)

func TestPublishMessage(t *testing.T) {
	type args struct {
		data string
	}

	var testData = []struct {
		name string
		args args
	}{
		{
			name: "add sample 1",
			args: args{
				data: fmt.Sprintf("data-%d", time.Now().UnixNano()),
			},
		},
		{
			name: "add sample 2",
			args: args{
				data: fmt.Sprintf("data-%d", time.Now().UnixNano()),
			},
		},
		{
			name: "add sample 3",
			args: args{
				data: fmt.Sprintf("data-%d", time.Now().UnixNano()),
			},
		},
		{
			name: "add sample 4",
			args: args{
				data: fmt.Sprintf("data-%d", time.Now().UnixNano()),
			},
		},
	}
	for _, tt := range testData {
		t.Run(tt.name, func(t *testing.T) {
			producer, err := nsq.NewProducer(os.Getenv("NSQ_SERVER"), nsq.NewConfig())
			if err != nil {
				t.Error(err)
				return
			}

			err = producer.Publish(os.Getenv("TOPIC_NAME"), []byte(tt.args.data))
			if err != nil {
				t.Error(err)
				return
			}
		})
	}
}
